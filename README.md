# naala89/apiopenstudio-nginx-php-7.4

This image provides a docker image for use in GitLab CI to test ApiOpenStudio
against PHP7.4 on NGINX.

It contains:

* NGINX
* PHP 7.4
* All PHP dependencies required by ApiOpenStudio
* composer
* rsync
* mysql client
* mariadb client
* openssl

## Getting Started

These instructions will cover usage information and for the docker container

### Prerequisites

In order to run this container you'll need docker installed.

* [Windows][docker_windows]
* [OS X][docker_osx]
* [Linux][docker_linux]

### Usage

#### Environment Variables

* MYSQL_HOST - mysql Host.
* MYSQL_DATABASE - ApiOpenStudio DB name.
* MYSQL_USER - ApiOpenStudio DB username.
* MYSQL_PASSWORD - ApiOpenStudio DB password.
* MYSQL_ROOT_PASSWORD- ApiOpenStudio DB root password.

#### Volumes

* ```/www``` - Nginx docroot.

#### Useful File Locations

* ```/etc/nginx/nginx.conf``` - The main Nginx config file.
* ```/etc/php7/conf.d/``` - PHP core directives.
* ```/etc/php7/php-fpm.d/``` - PHP-FPM.
* ```/etc/nginx/conf.d/	``` - Nginx.

#### Example

Run the container.

```shell
docker run --rm -v .:/var/www/ -e MYSQL_HOST=mariadb \
-e MYSQL_DATABASE=apiopenstudio -e MYSQL_USER=apiopenstudio \
-e MYSQL_PASSWORD=apiopenstudio -e MYSQL_ROOT_PASSWORD=apiopenstudio \
naala89/apiopenstudio-nginx-php-7.4:latest
```

## Find Us

* [GitLab][gitlab_apiopenstudio]
* [GitHub][github_naala89]
* [www.apiopenstudio.com][web_apiopenstudio]

## Versioning

The unstable tags are tagged with the Gitlab Pipeline ID.

Version tags (i.e. ```naala89/apiopenstudio:1.0```) references the version of
the image.

The ```latest``` tag is master branch, his will be based on the most recent tag.


## Authors

* **John Avery** - [John - GitLab][gitlab_john]

## License

This project is licensed under the MIT License - see the
[LICENSE.md][license] file for details.

## Acknowledgments

Many thanks to:

* [Existenz][existenz] for creating the great
  [existenz/webstack][existenz_webstack] base image this uses.

[docker_windows]: https://docs.docker.com/windows/started
[docker_osx]: https://docs.docker.com/mac/started/
[docker_linux]: https://docs.docker.com/linux/started/
[gitlab_apiopenstudio]: https://gitlab.com/apiopenstudio
[gitlab_john]: https://gitlab.com/john89
[github_naala89]: https://github.com/naala89
[web_apiopenstudio]: https://www.apiopenstudio.com
[license]: https://gitlab.com/apiopenstudio/docker_images/nginx_php_7.4/-/blob/master/LICENSE.md
[existenz]: https://hub.docker.com/u/existenz
[existenz_webstack]: https://hub.docker.com/r/existenz/webstack
